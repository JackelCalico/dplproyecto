let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let leaderSchema = new Schema({

    name : String,

    image: String,

    designation: String,

    abbr: String,

    featured: Boolean,

    description: String


});

leaderSchema.statics.allLeaders = function (cb){

    return this.find({},cb)
}

leaderSchema.statics.add = function(NewLeader,cb){

return this.create(NewLeader,cb);
}

leaderSchema.statics.updateById = function(aLeader, cb) {

    return this.updateOne(aLeader,cb);

};

leaderSchema.statics.removeById = function(NoLeader, cb) {

    return this.deleteOne({

        _id:NoLeader
        },cb);

};

module.exports = mongoose.model ("Leader",leaderSchema);