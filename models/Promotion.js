let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let promotionSchema = new Schema({

    name : String,

    image: String,

    label: String,

    price: Number,

    featured: Boolean,

    description: String


});

promotionSchema.statics.allPromotions = function (cb){

    return this.find({},cb)
}

promotionSchema.statics.add = function(NewPROMOTION,cb){

return this.create(NewPROMOTION,cb);
}

promotionSchema.statics.updateById = function(aPromotion, cb) {

    return this.updateOne(aPromotion,cb);

};

promotionSchema.statics.removeById = function(NoPromotion, cb) {

    return this.deleteOne({

        _id:NoPromotion
        },cb);

};

module.exports = mongoose.model ("Promotion",promotionSchema);