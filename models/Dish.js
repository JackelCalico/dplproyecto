let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let dishSchema = new Schema({

    name : String,

    image: String,

    category: String,

    label: String,

    price: Number,

    featured: Boolean,

    description: String,

    comments:[{

        rating: Number,

        comment: String,

        author: String,

        date: Date
    }]


});

//Lista de platos

dishSchema.statics.allDishes = function (cb){

    return this.find({},cb)
}

//Creacion plato

dishSchema.statics.add = function(NewDISH,cb){

return this.create(NewDISH,cb);
}

//Actualizar plato

dishSchema.statics.updateById = function(aDish, cb) {

    return this.updateOne(aDish,cb);

};

//Borrar plato


dishSchema.statics.removeById = function(bDish, cb) {

    return this.deleteOne({

        _id:bDish
        },cb);

};

//Creacion de comentario

dishSchema.statics.findUpdate = function(anId, data, cb) {
    return this.findByIdAndUpdate(
      {
        _id: anId
      },
      data,
      cb
    );
  };

  //Actualizacion comentario

  dishSchema.statics.findUpdateComments = function(Id, commentId, data, cb) {
    return this.findOneAndUpdate(
      {
        _id: Id,
        "comments._id": commentId
      },
      data,
      cb
    );
  };
  


module.exports = mongoose.model ("Dish",dishSchema);