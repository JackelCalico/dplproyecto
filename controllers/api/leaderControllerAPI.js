let Leader = require("../../models/Leader");

exports.leaders_list = function(req,res){

    Leader.allLeaders(function(err,leaders){

        if(err)
        res.status(500).send(err.message);
        res.status(200).send(leaders);
    })
}

exports.leaders_create = function(req,res){

    let leader = ({

        name: req.body.name,
        image: req.body.image,
        designation: req.body.designation,
        abbr: req.body.abbr,
        featured: req.body.featured,
        description: req.body.description
      })

      Leader.add(leader,(err,NLeader)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(NLeader); //Redirecciono al index
  
      });
}


exports.leaders_update = function(req,res){

    let leader = ({

        name: req.body.name,
        image: req.body.image,
        designation: req.body.designation,
        abbr: req.body.abbr,
        featured: req.body.featured,
        description: req.body.description
      })

      Leader.update(leader,(err,NLeader)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(NLeader); //Redirecciono al index
  
      });

      exports.leader_delete = function(req,res){

        //Borrado con la funcion remove
        Leader.removeById(req.params._id,(err,NoLeader)=>{
            if(err)
        
              res.status(500).send(err.message);
              res.status(200).send(NoLeader);
        
              
        
        });
    
    };
}