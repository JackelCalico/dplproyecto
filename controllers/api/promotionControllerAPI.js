let Promotion = require("../../models/Promotion");

exports.promotions_list = function(req,res){

    Promotion.allPromotions(function(err,promotions){

        if(err)
        res.status(500).send(err.message);
        res.status(200).send(promotions);
    })
}

exports.promotions_create = function(req,res){

    let promotion = ({

        name: req.body.name,
        image: req.body.image,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
      })

      Promotion.add(promotion,(err,Npromotion)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(Npromotion); //Redirecciono al index
  
      });
}


exports.promotions_update = function(req,res){

    let promotion = ({

        name: req.body.name,
        image: req.body.image,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
      })

      Promotion.update(promotion,(err,Npromotion)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(Npromotion); //Redirecciono al index
  
      });

      exports.promotion_delete = function(req,res){

        //Borrado con la funcion remove
        Promotion.removeById(req.params._id,(err,NoPromotion)=>{
            if(err)
        
              res.status(500).send(err.message);
              res.status(200).send(NoPromotion);
        
              
        
        });
    
    };
}