let Dish = require("../../models/Dish");

//Lista de platos

exports.dishes_list = function(req,res){

    Dish.allDishes(function(err,dishes){

        if(err)
        res.status(500).send(err.message);
        res.status(200).send(dishes);
    })
}

//Crear plato

exports.dishes_create = function(req,res){

    let dish = ({

        name: req.body.name,
        image: req.body.image,
        category: req.body.category,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
        
      })

      Dish.add(dish,(err,Ndish)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(Ndish); //Redirecciono al index
  
      });
}


//Funciones relacionadas con los comentarios

//Crear comentario

exports.dishAddComment = function(req,res){


  Dish.findUpdate(req.params._id,
    {
      $push: {
        comments: [
          {
            rating: req.body.rating,
            comment: req.body.comment,
            author: req.body.author,
            date: req.body.date
          }
        ]
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};


//Borrar comentario


exports.dish_deleteComment = function(req, res) {
  Dish.findUpdate(
    req.params._id,
    {
      $pull: {
        comments: {
          _id: [req.body.comentario_id]
        }
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Actualizar comentario

exports.dish_updateComment = function(req, res) {
  Dish.findUpdateComments(
    req.params._id,
    req.body.comment_id,
    {
      $set: {
        "comments.$.rating": req.body.rating,
        "comments.$.comment": req.body.comment,
        "comments.$.author": req.body.author,
        "comments.$.date": req.body.date
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};




exports.dishes_update = function(req,res){

    let dish = ({

        name: req.body.name,
        image: req.body.image,
        category: req.body.category,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
      })

      Dish.update(dish,(err,Ndish)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(201).send(Ndish); //Redirecciono al index
  
      });

      exports.dish_delete = function(req,res){

        //Borrado con la funcion remove
        Dish.removeById(req.params._id,(err,NoBici)=>{
            if(err)
        
              res.status(500).send(err.message);
              res.status(200).send(NoBici);
        
              
        
        });
    
    };
}