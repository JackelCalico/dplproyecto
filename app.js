var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var jwt = require("jsonwebtoken");


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');


//Rutas proyecto

var dishesAPIRouter = require('./routes/api/dishes');
var promotionsAPIRouter = require('./routes/api/promotions');
var leadersAPIRouter = require('./routes/api/leaders');
var authAPIRouter = require('./routes/api/auth');
var tokensAPIRouter = require('./routes/api/tokens');
var usuariosAPIRouter = require('./routes/api/usuarios');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//Token

app.set('secretkey','JWT_PWD_!!223344');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//Use del proyecto
app.use('/api/auth', authAPIRouter);
app.use('/api/dishes',validarUsuario,dishesAPIRouter);
app.use('/api/promotions',promotionsAPIRouter);
app.use('/api/leaders',leadersAPIRouter);
app.use('/api/tokens',tokensAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Funcion de validacion de usuario

function validarUsuario(req, res, next) {

  jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function(err, decoded){
 
  if (err) {
 
  res.json({status:"error", message: err.message, data:null});
 
  } else {
 
  req.body.userId = decoded.id;
 
  console.log('jwt verify: ' + decoded);
 
  next();
 
  }
 
 });
}

//conexion mongoose

mongoose.connect('mongodb://localhost/ProyectoDpl', { useUnifiedTopology: true, useNewUrlParser: true  }); 

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)

db.on("error", console.error.bind('Error de conexión con MongoDB'));

module.exports = app;
