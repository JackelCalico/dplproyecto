let express = require('express');
let router = express.Router();
let dishControllerAPI = require("../../controllers/api/dishControllerAPI");


//Lista de platos

router.get("/", dishControllerAPI.dishes_list);

//Crear plato

router.post("/create", dishControllerAPI.dishes_create);

//Actualizar platos

router.put("/:_id/update", dishControllerAPI.dishes_update);

//Borrar datos

router.delete("/:_id/delete", dishControllerAPI.dishes_update);

//Crear comentario del plato

router.post("/:_id/createComment",dishControllerAPI.dishAddComment);

//Actualizar comentario plato
router.put("/:_id/updateComment",dishControllerAPI.dish_updateComment);
//Borrar comentario plato
router.delete("/:_id/deleteComment",dishControllerAPI.dish_deleteComment);
module.exports = router;


//leaders