let express = require('express');
let router = express.Router();
let promotionControllerAPI = require("../../controllers/api/promotionControllerAPI");

router.get("/", promotionControllerAPI.promotions_list);

router.post("/create", promotionControllerAPI.promotions_create);

router.put("/:_id/update", promotionControllerAPI.promotions_update);

router.delete("/:_id/delete", promotionControllerAPI.promotions_update);
module.exports = router;

