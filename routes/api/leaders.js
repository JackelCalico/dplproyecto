let express = require('express');
let router = express.Router();
let leaderControllerAPI = require("../../controllers/api/leaderControllerAPI");

router.get("/", leaderControllerAPI.leaders_list);

router.post("/create", leaderControllerAPI.leaders_create);

router.put("/:_id/update", leaderControllerAPI.leaders_update);

router.delete("/:_id/delete", leaderControllerAPI.leaders_update);
module.exports = router;

